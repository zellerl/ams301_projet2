#include "headers.hpp"
#include <math.h>
#include <numeric>

int myRank;
int nbTasks;

int main(int argc, char *argv[]) {

  // 1. Initialize MPI
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &nbTasks);

  // 2. Read the mesh, and build lists of nodes for MPI exchanges, local
  // numbering
  Mesh mesh;
  readMsh(mesh, "benchmark/mesh.msh");
  buildListsNodesMPI(mesh);

  // 3. Build problem (vectors and matrices)
  ScaVector uNum(mesh.nbOfNodes);
  ScaVector uExa(mesh.nbOfNodes);
  ScaVector f(mesh.nbOfNodes);
  double a = 2;
  double b = 4;
  for (int i = 0; i < mesh.nbOfNodes; ++i) {
    double x = mesh.coords(i, 0);
    double y = mesh.coords(i, 1);
    uNum(i) = 0.;
    // uExa(i) = x + y;
    uExa(i) = cos(4 * M_PI * x / a) * cos(M_PI * x / b);
    // f(i) = x + y;
    f(i) = 1 * cos(4 * M_PI * x / a) * cos(M_PI * y / b) +
           (4 * M_PI / a) * sin(4 * M_PI * x / a) * cos(M_PI * y / b) -
           (M_PI / b) * cos(4 * M_PI * x / a) * sin(M_PI * y / b);
  }

  Problem pbm;
  double alpha = 1;
  buildProblem(pbm, mesh, alpha, f);

  // 4. Solve problem
  double tol = 1e-9; // (Currently useless)
  int maxit = 1000;
  jacobi(pbm.A, pbm.b, uNum, mesh, tol, maxit);

  // 5. Compute error and export fields
  ScaVector uErr = uNum - uExa;
  exportFieldMsh(uNum, mesh, "solNum", "benchmark/solNum.msh");
  exportFieldMsh(uExa, mesh, "solRef", "benchmark/solExa.msh");
  exportFieldMsh(uErr, mesh, "solErr", "benchmark/solErr.msh");

  double E_L2_loc_sq;
  double E_L2_glo_sq;

  ScaVector Mdu = pbm.M * uErr;
  E_L2_loc_sq = uErr.transpose() * Mdu; // uErr'*M*uErr

  MPI_Reduce(&E_L2_loc_sq, &E_L2_glo_sq, 1, MPI_DOUBLE, MPI_SUM, 0,
             MPI_COMM_WORLD);

  if (myRank == 0) {
    double E_L2 = sqrt(abs(E_L2_glo_sq));
    cout << "norme L2 de l'erreur " << E_L2 << "\n";
  }

  // 6. Finilize MPI
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  return 0;
}