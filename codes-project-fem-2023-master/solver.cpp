#include "headers.hpp"

extern int myRank;
extern int nbTasks;

//================================================================================
// Solution of the system Au=b with Jacobi
//================================================================================

void jacobi(SpMatrix &A, ScaVector &b, ScaVector &u, Mesh &mesh, double tol,
            int maxit) {
  if (myRank == 0)
    cout << "== jacobi" << endl;

  // Compute the solver matrices
  int size = A.rows();
  ScaVector Mdiag(size);
  SpMatrix N(size, size);
  for (int k = 0; k < A.outerSize(); ++k) {
    for (SpMatrix::InnerIterator it(A, k); it; ++it) {
      if (it.row() == it.col())
        Mdiag(it.row()) = it.value();
      else
        N.coeffRef(it.row(), it.col()) = -it.value();
    }
  }
  exchangeAddInterfMPI(Mdiag, mesh);

  // Jacobi solver
  double residuNorm = 1e2;
  double dist_it = 0;
  double dist_it_glo = 0;
  double tmp = 0; // variable temporaire de stockage de valeur
  int it = 0;
  while (residuNorm > tol && it < maxit) {
    dist_it = 0;
    // Compute N*u
    ScaVector Nu = N * u;
    exchangeAddInterfMPI(Nu, mesh);

    // Update field and distance to precedent iteration
    for (int i = 0; i < size; i++) {
      // cout << i << " " << mesh.nodesMinExch(i) << " / ";
      tmp = u(i);
      u(i) = 1 / Mdiag(i) * (Nu(i) + b(i));
      if (mesh.nodesMinExch(i) == -1 || mesh.nodesMinExch(i) == myRank) {
        dist_it += pow(u(i) - tmp, 2);
        // cout << mesh.nodesMinExch(i) << " passed ";
      } else {
        // cout << "\n" << myRank << " " << i << " " << mesh.nodesMinExch(i) <<
        // " not passed ";
      }
    }

    // Update residual and iterator
    if ((it % 100) == 0) {
      MPI_Allreduce(&dist_it, &dist_it_glo, 1, MPI_DOUBLE, MPI_SUM,
                    MPI_COMM_WORLD);
      residuNorm = sqrt(dist_it_glo);
      if (myRank == 0)
        cout << "   [" << it << "] residual: " << residuNorm << " local res "
             << sqrt(dist_it) << endl;
    }
    it++;
  }

  if (myRank == 0) {
    cout << "   -> final iteration: " << it << " (prescribed max: " << maxit
         << ")" << endl;
    cout << "   -> final residual: " << residuNorm
         << " (prescribed tol: " << tol << ")" << endl;
  }
}
